package pe.esao.webservice.entity;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "tabla_personas", uniqueConstraints = { @UniqueConstraint(columnNames = "identificacion") })
@Cacheable(true)
public class Persona implements Serializable{

	private static final long serialVersionUID = 6626647004732853891L;
	
	@Id
	@Column(unique = true, nullable = false)
	private int id;
	
	@Column(length = 10)
	private String identificacion;
	@Column(length = 30)
	private String nombres;
	@Column(length = 30)
	private String apellidos;
	@Column(length = 30)
	private String edad;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIdentificacion() {
		return identificacion;
	}
	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getEdad() {
		return edad;
	}
	public void setEdad(String edad) {
		this.edad = edad;
	}
	
	
}
